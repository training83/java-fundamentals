import java.util.Random;

/**
 * Created by u.7829872 on 26/10/2020.
 */
public class Dice {

    public static int rollDice() {

        int diceScore = 5;
        Random random = new Random();
        diceScore = random.nextInt(6) + 1;
        return diceScore;
    }


    public static void main(String[] args) {
        int result = rollDice();
        System.out.println("You rolled: " + result);
    }


}
