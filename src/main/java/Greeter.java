/**
 * Created by u.7829872 on 26/10/2020.
 */
public class Greeter {

    public static String createGreeting(String name) {
        String greeting = "Hello " + name;
        return greeting;
    }

    public static void main(String[] args) {
        String name = args[0];
        String greet = createGreeting(name);
        System.out.println(greet);
    }
}
