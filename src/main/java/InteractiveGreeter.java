import java.util.Scanner;

/**
 *
 * InteractiveGreeter
 *
 * This class reads user input and presents a custom message.
 *
 * Created by u.7829872 on 02/11/2020.
 */
public class InteractiveGreeter {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        System.out.println("What is your name?");

        String name = s.nextLine();

        System.out.println("Hello " + name);

    }
}
