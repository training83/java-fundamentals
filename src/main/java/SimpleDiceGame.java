/**
 * Created by u.7829872 on 26/10/2020.
 */
public class SimpleDiceGame {

    public static void main(String[] args) {
        String greet = Greeter.createGreeting("Jon");
        System.out.println(greet);

        int result = Dice.rollDice();
        System.out.println("You rolled " + result);

        if (result >3)
        System.out.println("You Win");
        else System.out.println("You Lose");
    }
}
