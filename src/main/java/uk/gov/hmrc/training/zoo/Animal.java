package uk.gov.hmrc.training.zoo;

/**
 * Animal
 *
 * Java data object
 *
 * Conforms to the Java Beans convention by using strict encapsulation.
 *
 * Created by u.7829872 on 02/11/2020.
 */
public class Animal {

    //Properties
    private String type;
    private String name;
    private int age;

    //Methods
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
