package uk.gov.hmrc.training.zoo;

/**
 * Created by u.7829872 on 02/11/2020.
 */
public class Zoo {
    public static void main(String[] args) {
        
        Animal tiger = new Animal();
        tiger.setName("Tim");
        tiger.setType("Tiger");
        tiger.setAge(5);

        Animal dog = new Animal();
        dog.setName("Dot");
        dog.setType("Dog");
        dog.setAge(3);

        System.out.println("Hello " + tiger.getName() + ". Welcome to our zoo");
        System.out.println("Hello " + dog.getName() + ". Welcome to our zoo");
    }
}
